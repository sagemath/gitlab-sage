import axios from 'axios';

export async function commit(source: string, kind: string, ref: string) {
    return (await axios.get(`/api/commit/${source}/${kind}`, { params: { ref }})).data;
}

export async function releases() {
    return (await axios.get('/api/releases')).data;
}

export async function binders(source: string, kind: string, ref: string) {
    return (await axios.get(`/api/binders/${source}/${kind}`, { params: { ref }})).data;
}

export async function pipelines(source: string, ref: string) {
    return (await axios.get(`/api/pipelines/${source}/commit`, { params: { ref } })).data;
}

export async function remove(branch: string) {
    return await axios.delete(`/api/binder`, { params: { branch }});
}
