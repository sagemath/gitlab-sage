declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module 'vue-loading-overlay' {
	const Loading: any;
	export default Loading;
}
