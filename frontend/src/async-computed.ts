import AsyncComputedPlugin, { IAsyncComputedProperty } from 'vue-async-computed';
import { createDecorator, VueDecorator } from 'vue-class-component';
import Vue from 'vue';

Vue.use(AsyncComputedPlugin as any);

// A decorator which turns a method returning a promise into an asynchronously computed prop with vue-async-computed.
export default function AsyncComputed<T>(computedOptions?: Omit<IAsyncComputedProperty<T>, 'get'>): VueDecorator {
  return createDecorator((options, key) => {
    (options as any).asyncComputed = (options as any).asyncComputed || {};
    const method = options.methods![key];
    (options as any).asyncComputed[key] = {
      get: method,
      ...computedOptions,
    } as IAsyncComputedProperty<T>;
    delete options.methods![key];
  });
}
