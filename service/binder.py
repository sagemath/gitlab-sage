r"""
Binder Notebooks from SageMath Docker images
"""
#############################################################################
#        Copyright (C) 2019      Julian Rüth <julian.rueth@fsfe.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#############################################################################
from flask import render_template
from .gitlab import session
from gitlab.exceptions import GitlabGetError
import urllib.parse

def create_binders(source, kind, ref):
    images = search_registry(source, kind, ref)
    if not images:
        return []

    for image in images:
        create_binder(source, kind, ref, image)
    return images


def create_binder(source, kind, ref, image):
    from .api import BINDER_PROJECT
    project = session(authenticated=True).projects.get(BINDER_PROJECT)

    name = "%s/%s/%s" % (source, kind, ref)

    try:
        branch = project.branches.get(name)
        commit = branch.commit["id"]
    except GitlabGetError as e:
        if e.response_code != 404:
            raise
        branch = None

    action = "create" if branch is None else "update"
    notebook = "SageMath.ipynb"
    if branch is None or kind == "branch":
        location = image["location"]
        replacements = {
            "image": location,
            "kind": kind,
            "ref": ref,
            "python": 3 if location.endswith("-py3") else 2,
        }
        files = {
            "Dockerfile": render_template("Dockerfile", **replacements),
            notebook: render_template("%s.ipynb" % (kind,), **replacements)
        }
        data = {
            'branch': name,
            'commit_message': "render binder configuration",
            'actions': [{
                'action': action,
                'file_path': file_path,
                'content': content,
                } for (file_path, content) in files.items()]
        }

        if branch is None:
            data["start_branch"] = "master"

        commit = project.commits.create(data).id

    image["is_new"] = branch is None
    image["url"] = create_binder_url(project, commit, notebook)
    image["branch"] = name
    return image


def remove_binder(name):
    from .api import BINDER_PROJECT
    project = session(authenticated=True).projects.get(BINDER_PROJECT)
    project.branches.delete(name)
    return "Deleted", 200


def create_binder_url(project, commit, notebook):
    return "https://mybinder.org/v2/gl/%s/%s?filepath=%s" % (urllib.parse.quote(project.path_with_namespace, safe=''), urllib.parse.quote(commit, safe=''), urllib.parse.quote(notebook, safe=''))


def create_branch(project, name):
    try:
        branch = project.branches.get(name)
        return (branch, False)
    except GitlabGetError as e:
        if e.response_code != 404:
            raise

    branch = project.branches.create({'branch': name, 'ref': 'master'})
    return (branch, True)


def search_registry(source, kind, ref):
    from .api import PROJECTS, IMAGES
    source = PROJECTS[source]

    # Search for the sagemath repository (not the sagemath-dev repository)
    repositories = [
      repo for repo
      in session().projects.get(source).repositories.list(all=True)
      if repo.name in IMAGES]
    print(repositories)
    if len(repositories) == 0:
        return []

    import itertools
    return list(itertools.chain(*[
        search_repository(repository, kind, ref)
        for repository in repositories]))


def search_repository(repository, kind, ref):
    ref = normalize(ref)
    if kind in ["commit", "tag", "branch"]:
        if kind == "branch" and ref == "master":
            ref = "latest"
        return [tag for tag in [
                get_tag(repository, ref),
                get_tag(repository, ref + "-py3")] if tag]
    raise NotImplementedError(kind)


def get_tag(repository, tag):
    try:
        return repository.tags.get(id=tag).attributes
    except GitlabGetError as e:
        if e.response_code == 404:
            return None
        raise


def push(branch, previous_commit, images, message, badge):
    raise NotImplementedError("push()")


def normalize(ref):
    r"""
    Apply the normalization that our .ci/update-env.sh script performs to the
    branch/tag name ``ref``.
    """
    import re
    print(ref)
    ref = re.sub('\s*', '', ref)
    ref = re.sub('[^a-zA-Z0-9_.-]+', '-', ref)
    ref = re.sub('^[-.]*', '', ref)
    print(ref)
    return ref
