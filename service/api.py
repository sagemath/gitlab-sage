r"""
Entrypoint of the web application

This application provides a /hook for GitLab web hooks and a number of web
pages that invoke GitLab's API.
"""
#############################################################################
#        Copyright (C) 2019      Julian Rüth <julian.rueth@fsfe.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#############################################################################
import os
from flask import Flask, request, abort, jsonify, json
from flask_cors import CORS

from .binder import create_binders, remove_binder
from .gitlab import session

TOKEN = os.environ.get("TOKEN")
GIT_BINDER_URL = os.environ.get("GIT_BINDER_URL")
ENV = os.environ.get("ENV", "production")
PROJECTS = json.loads(os.environ.get("PROJECTS", '''{
    "sage": 6249557,
    "dev": 6249701,
    "trac": 6249490
}'''))
PROJECT = PROJECTS[os.environ.get("PROJECT", "sage")]
IMAGES = json.loads(os.environ.get("IMAGES", '["sagemath", "sagemath-py3"]'))
BINDER_PROJECT = os.environ.get("BINDER_PROJECT", 13920331)

APP = Flask(__name__)
APP.config.from_object(__name__)
CORS(APP, resources={r'/*': {'origins': '*'}})


@APP.context_processor
def globals_for_all_templates():
    r"""
    Make some variables available in all HTML templates without explicitly
    injecting them.
    """
    return {"environment": ENV}


@APP.route("/api/health", methods=["GET"])
def health():
    r"""
    Provide a pingable interface which just returns a 200 to check that our
    service is running.
    """
    return "healthy"


@APP.route("/api/releases", methods=["GET"])
def releases():
    r"""
    Return a list of recent releases and pre-releases, i.e., the tags of the
    public repository.
    """
    return jsonify([
        tag.name for tag in
        session().projects.get(PROJECT).tags.list(per_page=40)])


@APP.route("/api/commit/<source>/<kind>", methods=["GET"])
def sha(source, kind):
    ref = request.args.get("ref")
    source = PROJECTS[source]

    if kind == 'commit':
        return session().projects.get(source).commits.get(ref).attributes
    elif kind == 'tag':
        return session().projects.get(source).tags.get(ref).commit
    elif kind == 'branch':
        return session().projects.get(source).branches.get(ref).commit
    else:
        return "Unsupported kind", 400


@APP.route("/api/hook", methods=["GET"])
def hook():
    r"""
    Callback for GitLab hooks calling into this application.

    Currently, nothing is implemneted here, see
    https://docs.gitlab.com/ee/user/project/integrations/webhooks.html for
    possible hooks.
    """
    if not TOKEN or request.headers.get('X-Gitlab-Token') != TOKEN:
        abort(403)
    if not request.is_json:
        return "JSON payload expected.", 400
    return "No hooks implemented yet.", 404


@APP.route("/api/binders/<source>/<kind>", methods=["GET"])
def binders(source, kind):
    r"""
    Push a commit to ``GIT_BINDER_URL`` which can be pointed to by mybinder.org
    to spawn ``image``.

    Return the SHA of the created commit.
    """
    return jsonify(
        create_binders(source=source, kind=kind, ref=request.args.get('ref')))


@APP.route("/api/pipelines/<source>/commit", methods=["GET"])
def pipelines(source):
    ref = request.args.get("ref")
    source = PROJECTS[source]

    return jsonify([pipeline.attributes for pipeline
            in session().projects.get(source).pipelines.list(sha=ref)])

@APP.route("/api/binder", methods=["DELETE"])
def remove():
    branch = request.args.get("branch")
    return remove_binder(branch)
