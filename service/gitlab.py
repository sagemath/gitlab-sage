r"""
Session factories for gitlab-python
"""
#############################################################################
#        Copyright (C) 2019      Julian Rüth <julian.rueth@fsfe.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#############################################################################
import os
import gitlab

GITLAB = os.environ.get("GITLAB", "https://gitlab.com")
TOKEN = os.environ.get("GITLAB_TOKEN")


def session(authenticated=False):
    r"""
    Return a non-authenticated interface to GitLab.
    """
    if authenticated:
        return gitlab.Gitlab(GITLAB, private_token=TOKEN)
    return gitlab.Gitlab(GITLAB)
