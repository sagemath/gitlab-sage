#############################################################################
#        Copyright (C) 2019      Julian Rüth <julian.rueth@fsfe.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#############################################################################

from .fixture import TOKEN, client

HEADERS = {'X-Gitlab-Token': TOKEN,
           'X-Gitlab-Event': 'Pipeline Hook'}


def test_auth(client):
    r"""
    Test that authentication is properly checked for all routes that reuqire
    authentication.
    """
    response = client.get("/api/hook")
    assert response.status_code == 403


def test_syntax(client):
    r"""
    Test that we correctly detect JSON parameters.
    """
    response = client.get("/api/hook", headers=HEADERS)
    assert response.status_code == 400
