# Automating Tasks Through GitLab Hooks and By Talking to GitLab's API

This is a web application that interacts with GitLab. It is meant to be
deployed as a container with the provider [Dockerfile](./Dockerfile).

For local development, you should start the flask server for the API backend,
and the webpack dev server for the frontend.
```
GITLAB_TOKEN=? ENV=development python -m service
cd frontend && yarn serve
```
