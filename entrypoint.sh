#!/bin/bash
set -e

export FLASK_PORT=$(( PORT + 1 ))
envsubst '$PORT,$FLASK_PORT' < /etc/nginx/nginx.conf.in > /etc/nginx/nginx.conf

gunicorn --bind :$FLASK_PORT --daemon --workers 1 --threads 8 service.api:APP
wget --retry-connrefused http://localhost:$FLASK_PORT/api/health

exec nginx
