FROM node:alpine AS build
WORKDIR /frontend
RUN apk add git
COPY frontend/. .
ENV NODE_OPTIONS=--openssl-legacy-provider
RUN yarn
RUN yarn build

FROM python:3.7-alpine
WORKDIR /app
ENV PORT=8080
ENV TOKEN=

COPY requirements.txt .

RUN pip install --requirement requirements.txt
RUN apk add nginx gettext wget

COPY service/. ./service
COPY --from=build /frontend/dist/. ./frontend

COPY nginx.conf.in /etc/nginx/nginx.conf.in
RUN mkdir -p /run/nginx

COPY entrypoint.sh .

CMD sh /app/entrypoint.sh
